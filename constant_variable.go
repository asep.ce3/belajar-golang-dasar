package main

import "fmt"

func main() {
	const hello = "hello world"

	const (
	firstName = "Uzumaki"
	lastName = "Naruto"
	)

	fmt.Println(hello)
	fmt.Println(firstName + " " + lastName)
}
