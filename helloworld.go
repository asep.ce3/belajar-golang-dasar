package main

import "fmt"

func main() {
	var hello = "hello world"

	var (
		firstName = "Uzumaki"
		lastName = "Naruto"
	)
	fmt.Println(len(hello))
	fmt.Println(hello)
	fmt.Println(firstName + " " + lastName)
}