package main

import "fmt"

func main() {
	var names [2]string
	names[0] = "Naruto"
	names[1] = "Uzumaki"

	fmt.Println(names[0])
	fmt.Println(names[1])

	var values = [3]int {
		90,
		80,
		70,
	}
	fmt.Println(values)
	fmt.Println(len(values))

	// ubah data
	values[1] = 50
	fmt.Println(values)
}
