# Tutorial Golang

## Next
Tipe Data Map
https://docs.google.com/presentation/d/1QNFV9kjV4TfN-FVFLT6-8Urq2MmadAmgc1puk-YE5Fs/edit#slide=id.g76f41bdaac_0_442

## Sumber tutorial
```
https://www.udemy.com/course/pemrograman-go-lang-pemula-sampai-mahir/
```

## Install Golang
* Ubuntu
```bash
sudo apt install golang
```

## Buat Projek
```bash
belajar-golang-dasar
cd belajar-golang-dasar
```

## Run file Golang
* Build file
```bash
go build helloworld.go
```

* Run file
```bash
./helloworld
```

* Bisa juga langsung running tanpa build terlebih dahulu
```bash
go run helloworld.go
```

