package main

import "fmt"

func main() {
	/*
	array[low:high]
	- isinya diri indeks low sampai sebelum high
	 */

	names := [...]string {"Naruto", "Sasuke", "Sakura", "Chouji", "Shikamaru", "Ino"}
	slice := names[3:5]
	fmt.Println(slice)

	// length slice
	fmt.Printf("Length : %d\n", len(slice))

	// capacity slice
	fmt.Printf("Capacity : %d\n", cap(slice))

	// append slice
	newSlice := append(slice, "Kakashi")
	fmt.Println(newSlice)
}
