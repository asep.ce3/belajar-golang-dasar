package main

import "fmt"

func main() {

	// alias untuk tipe data string
	type NoKTP string

	var ktpNaruto NoKTP = "1111111"
	fmt.Println(ktpNaruto)
	fmt.Println(NoKTP("2222222"))
}
